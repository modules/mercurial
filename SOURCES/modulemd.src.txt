document: modulemd
version: 2
data:
    # From mercurial.spec Summary
    summary: Mercurial -- a distributed SCM
    # From mercurial.spec description
    description: >-
            Mercurial is a fast, lightweight source control management system designed
            for efficient handling of very large distributed projects.
    license:
        module:
            - MIT
    references:
        community: https://www.mercurial-scm.org/
        documentation: https://www.mercurial-scm.org/guide
        tracker: https://www.mercurial-scm.org/wiki/BugTracker
    dependencies:
        - buildrequires:
            platform: [el8]
            python27: [2.7]
          requires:
            platform: [el8]
            python27: [2.7]
    components:
        # SRPMs
        rpms:
            mercurial:
                # From mercurial.spec Summary
                rationale: Mercurial -- a distributed SCM
                ref: stream-mercurial-4.8
    api:
        # RPMs
        # From *.spec %package
        # *-doc are filtered.
        rpms:
            # From mercurial.spec
            - mercurial
            - mercurial-hgk
    profiles:
        common:
            rpms:
                - mercurial

